import 'package:flutter/material.dart';
import 'package:reg_profile/login.dart';
import 'academic_calendar.dart';
import 'contact.dart';

BuildContext? oldContext;
class Nevegationdrawer extends StatelessWidget {
  const Nevegationdrawer({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.blue.shade800),
            accountName: Text(
              "Thanwa Saensud",
              style:
                  TextStyle(color: Colors.white),
            ),
            accountEmail: Text(
              "63160006@go.buu.ac.th",
              style:
                  TextStyle(color: Colors.white),
            ),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(
                'assets/img/63160006.png',
              ),
            ),
          ),
          ListTile(
            title: const Text(
              'เข้าสู่ระบบ',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              oldContext= context;
              Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new LoginDemo()));
            },
          ),
          ListTile(
            title: const Text(
              'ปฎิทินการศึกษา',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new Table_calender()));
            },
          ),
          ListTile(
            title: const Text(
              'ติดต่อเรา',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new Contact()));
            },
          ),
          ListTile(
            title: const Text(
              'วิชาที่เปิดสอน',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {},
          ),
          ListTile(
            title: const Text(
              'หลักสูตรที่เปิดสอน',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {},
          ),
          ListTile(
            title: const Text(
              'ตารางสอนอาจารย์',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {},
          ),
          ListTile(
            title: const Text(
              'ค่าธรรมเนียมการศึกษา',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {},
          ),
          ListTile(
            title: const Text(
              'Download แบบฟอร์ม',
              style: TextStyle(fontSize: 20),
            ),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}

