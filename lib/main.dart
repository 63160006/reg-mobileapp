import 'package:flutter/material.dart';
import './drawer.dart';
BuildContext? buildOldContext;
void main() {
  runApp(MainApp());
}
BuildContext getBuildContextLogin(){
  return buildOldContext!;
}
class MainApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    buildOldContext = context;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(appBar: appBar, 
      body: mainPage(), drawer: Nevegationdrawer()),
    );
  }
}

class mainPage extends StatelessWidget {
  const mainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
  children: [
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          Center(
                child: Text(
                  "ยินดีต้อนรับเข้าสู่ระบบบริการศึกษา",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
          ListTile(
            title: Text(
              'ประกาศเรื่อง',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 20,
              ),
            ),
          ),
          ListTile(
            title: Text(
              '1.การยื่นคำร้องขอสำเร็จการศึกษา (ด่วน)',
              style: TextStyle(
                color: Colors.red,
                fontSize: 15,
              ),
            ),
          ),
          Image.asset('assets/img/new_img1.png'),
          Padding(padding: EdgeInsets.all(3.0),),
          
          ListTile(
            title: Text(
              '**นิสิตต้องยื่นคำร้องขอสำเร็จการศึกษาและชำระเงินตามกำหนดเวลา หากนิสิตไม่ได้ยื่นสำเร็จการศึกษาทางกองทะบียนฯ จะไม่เสนอชื่อสำเร็จการศึกษา',
              style: TextStyle(color: Colors.red),
            ),
          ),
          ListTile(
            title: Text(
              'คู่มือการยื่นคำร้องสำเร็จการศึกษา นิสิตพิมพ์ใบชำระเงินที่เมนูแจ้งจบและขึ้นทะเบียนนิสิต',
              style: TextStyle(color: Colors.black),
            ),
          ),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(1.0),
    ),
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565',
            style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              ),
            ),
          ),
          Image.asset('assets/img/new_img2.png'),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(1.0),
    ),
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('3.กำหนดยื่นคำร้องเกี่ยวกับงานพิธีพระราชื่นปริญญาบัตรปีการศึกษา 2563 และปีการศึกษา 2564',
            style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              ),
            ),
          ),
          Image.asset('assets/img/new_img3.png'),
          Padding(padding: EdgeInsets.all(3.0),),
          ListTile(
            title: const Text('ผู้ที่ได้รับอนุมัติเปลี่ยนคำนำหน้าคำอ่านปี 2563',
            style: TextStyle(
              color: Colors.blue,
              fontSize: 13,
              ),
            ),
          ),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(1.0),
    ),
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('4.แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี',
            style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              ),
            ),
          ),
          ListTile(
            title: const Text('   ขอเชิญนิสิตและผู้ปกครองร่วมทำแบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี',
            style: TextStyle(
              color: Colors.blue,
              fontSize: 13,
              ),
            ),
            subtitle: Text(
              '   - สำหรับนิสิต ที่ https://bit.ly/3cyvuuf',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Image.asset('assets/img/new_img4.png'),
          Padding(padding: EdgeInsets.all(3.0),),
          
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(1.0),
    ),
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('5.LINE Official'),
            subtitle: Text(
              '   เพิ่มเพื่อน LINE Official ของกองทะเบียนฯ พิมพ์ "@regbuu" (ใส่ @ ด้วย) หรือ https://lin.ee/uNohJQP หรือสแกน Qrcode ด้านล่าง',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Image.asset('assets/img/new_img5.png'),
          Padding(padding: EdgeInsets.all(3.0),),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(1.0),
    ),
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('6.Dowload ระเบียบสำหรับแนบเบิกค่าเล่าเรียนบุตร',
            style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              ),
            ),
          ),
          ListTile(
            title: const Text('   **สอบถามข้อมูลที่หองคลังฯ ชั้น 3 อาคารสำนักงานอธิการบดี (ภปร) โทร 038-102157**',
            style: TextStyle(
              color: Colors.red,
              fontSize: 18,
              ),
            ),
          ),
          Image.asset('assets/img/new_img6.png'),
          Padding(padding: EdgeInsets.all(3.0),),
        ],
      ),
    ),
  ],
);
  }
}



var appBar = AppBar(
  leading: Builder(
    builder: (BuildContext context) {
      return IconButton(
        icon: const Icon(
          Icons.menu,
          color: Colors.white,
          // size: 40, // Changing Drawer Icon Size
        ),
        onPressed: () {
          Scaffold.of(context).openDrawer();
        },
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      );
    },
  ),
  title: Container(
      child: Row(
    children: [
      Column(
        children: [
          Container(
            margin: const EdgeInsets.all(3),
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('assets/img/buu_icon.png'),
              ),
            ),
          )
        ],
      ),
      Column(
        children: [
          Text(
            "มหาวิทยาลัยบูรพา",
            style: TextStyle(fontSize: 20, color: Colors.yellow),
          ),
          Text(
            "Burapha University",
            style: TextStyle(fontSize: 15, color: Colors.white),
          ),
        ],
      ),
    ],
  )),
  backgroundColor: Colors.blue.shade800,
  elevation: 0.0,
);