import 'package:flutter/material.dart';
import 'main.dart';
import 'exam.dart';

class LoginDemo extends StatefulWidget {
  @override
  _LoginDemoState createState() => _LoginDemoState();
}

class _LoginDemoState extends State<LoginDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue.shade800,
        title: const Text("Login Page",style: TextStyle(color: Colors.white),),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 70.0),
              child: Center(
                child: Container(
                    width: 250,
                    height: 150,
                    child: Image.asset('assets/img/buu_icon.png')),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(255, 21, 101, 192))
                    ),
                    labelText: 'Username',
                    hintText: 'Username'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              child: TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(255, 21, 101, 192))
                    ),
                    labelText: 'Password',
                    hintText: 'Password'),
              ),
            ),
            const Padding(padding: EdgeInsets.all(20.0)),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.yellow, borderRadius: BorderRadius.circular(20)),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  // primary: Colors.grey,
                  elevation: 20,
                ),
                onPressed: () {
                  Navigator.pushReplacement(
                      context, new MaterialPageRoute(
                      builder: (context) => new MyApp()));
                },
                child: const Text(
                  'Sign in',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            const Padding(padding: EdgeInsets.all(10.0)),
            TextButton(
              child: const Text(
                'Forgot Password',
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
