import 'package:flutter/material.dart';

class Contact extends StatefulWidget {
  @override
  Contact_officer createState() => Contact_officer();
}

class Contact_officer extends State<Contact> {  
  @override  
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue.shade800,
        title: const Text("ติดต่อเรา",style: TextStyle(color: Colors.white)),  
          ),
          
          body: Center(  
              child: Column(children: <Widget>[  
                Container( 
                   width: double.infinity,
                  margin: EdgeInsets.all(20),  
                  child: Table(  
                    
                    // defaultColumnWidth: FixedColumnWidth(120.0),  
                    
                    border: TableBorder.all(  
                        color: Colors.white,  
                        style: BorderStyle.solid,  
                        width: 2),  
                    children: [  
                      TableRow( children: [  
                        Column(children:[Text('เวลาทำการ')]),  
                        Column(children:[Text('เปิดภาคเรียน\n - 08:30 - 19:30 น. (จันทร์ พุธ ศุกร์)\n - 08:30 - 16:30 น. (อังคาร พฤหัสบดี เสาร์ อาทิตย์)\n หมายเหตุ: ปิดวันหยุดราชการ\n ปิดภาคเรียน')]),  
                      ]),  
                      TableRow( children: [  
                        Column(children:[Text('ที่อยู่')]),    
                        Column(children:[Text('กองทะเบียนและประมวลผลการศึกษามหาวิทยาลัยบูรพา 169 ถ.ลงหาดบางแสน ต.แสนสุข อ.เมือง จ.ชลบุรี 20131')]),  
                      ]),  
                      TableRow( children: [  
                        Column(children:[Text('แผนที่')]),  
                        Column(children:[Text('	แผนที่')]),  
                      ]),  
                      TableRow( children: [  
                        Column(children:[Text('โทรศัพท์ (เคาน์เตอร์)')]),   
                        Column(children:[Text('0 3810 2725')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text('โทรสาร')]),   
                        Column(children:[Text('0 3839 0441,\n0 3810 2721')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text('โทรศัพท์ (เจ้าหน้าที่)')]),   
                        Column(children:[Text('ฝ่ายรับเข้าศึกษาระดับปริญญาตรี')]),  
                      ]),
                    ],  
                  ),  
                ),  
              ])  
          ));
  }  
}