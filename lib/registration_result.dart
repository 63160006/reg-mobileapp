import 'package:flutter/material.dart';
import 'drawer.dart';
import 'main.dart';
import 'exam.dart';


class Registration_Result extends StatefulWidget {
  var context;
  Registration_Result(context){
    this.context = context;
  }

  @override
  Registration createState() => Registration();
}

class Registration extends State<Registration_Result> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(appBar: appBar(widget.context),
      body: body,
      )
    );
  }
}

var body = ListView(
  children: [
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          Center(
                child: Text(
                  "ยินดีต้อนรับเข้าสู่ระบบบริการศึกษา",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
          ListTile(
            title: Text(
              '63160006 : นายธันวา แสนสุด : คณะคณะวิทยาการสารสนเทศ',
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
            ),
          ),
          ListTile(
            title: Text(
              'ผลลงทะเบียน',
              style: TextStyle(
                color: Colors.orange,
                fontSize: 20,
              ),
            ),
          ),
          ListTile(
            title: Text(
              'ปีการศึกษา 2565 / 1 2 ฤดูร้อน',
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
            ),
          ),
          Image.asset('assets/img/registration.png'),
        ],
      ),
    ),
  ],
);

AppBar appBar(ctx) {
  return AppBar(
    leading: Builder(
      builder: (BuildContext context) {
        return IconButton(
          icon: const Icon(
            Icons.exit_to_app_outlined,
            color: Colors.white,
            // size: 40, // Changing Drawer Icon Size
          ),
          onPressed: () {
            Navigator.push(
              ctx,
              new MaterialPageRoute(
                builder: (context) => new MainApp(),
              ),
            );
          },
        );
      },
    ),
    title: Container(
        child: Row(
      children: [
        Column(
          children: [
            Container(
              margin: const EdgeInsets.all(3),
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage('assets/img/buu_icon.png'),
                ),
              ),
            )
          ],
        ),
        Column(
          children: [
            Text(
              "มหาวิทยาลัยบูรพา",
              style: TextStyle(fontSize: 20, color: Colors.yellow),
            ),
            Text(
              "Burapha University",
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ],
        ),
      ],
    )),
    backgroundColor: Colors.blue.shade800,
    elevation: 0.0,
  );
}