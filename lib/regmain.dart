import 'package:flutter/material.dart';
import 'drawer.dart';
import 'exam.dart';
import 'main.dart';
class RegMain extends StatefulWidget {
  var context;
  RegMain(context){
    this.context = context;
  }
  @override
  Reg_StatefulWidgetState createState() => Reg_StatefulWidgetState();
}

class Reg_StatefulWidgetState extends State<RegMain> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: appBar(widget.context),
          body: body,
          // body: body, drawer: Nevegationdrawer()),
        ));
  }
}

var body = ListView(
  children: [
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          Center(
            child: Text(
              "ยินดีต้อนรับเข้าสู่ระบบบริการศึกษา",
              style: TextStyle(
                fontSize: 20,
              ),
              textAlign: TextAlign.start,
            ),
          ),
          ListTile(
            title: Text(
              '63160006 : นายธันวา แสนสุด : คณะคณะวิทยาการสารสนเทศ',
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
            ),
          ),
          ListTile(
            title: Text(
              'ประกาศเรื่อง',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 20,
              ),
            ),
          ),
          ListTile(
            title: Text(
              '1.แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการของสำนักงานอธิการบดี(ด่วนที่สุด)',
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
            ),
          ),
          Image.asset('assets/img/reg_new1.png'),
          Padding(
            padding: EdgeInsets.all(3.0),
          ),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(1.0),
    ),
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text(
              '2.การทำบัตรนิสิตกับธนาคารกรุงไทย',
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
            ),
          ),
          ListTile(
            title: const Text(
              'กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาทสำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา ส่วนนิสิตที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง',
              style: TextStyle(
                color: Colors.red,
                fontSize: 13,
              ),
            ),
          ),
          Image.asset('assets/img/reg_new2.png'),
          Image.asset('assets/img/reg_new3.png'),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(1.0),
    ),
  ],
);

AppBar appBar(ctx) {
  return AppBar(
    leading: Builder(
      builder: (BuildContext context) {
        return IconButton(
          icon: const Icon(
            Icons.exit_to_app_outlined,
            color: Colors.white,
            // size: 40, // Changing Drawer Icon Size
          ),
          onPressed: () {
            Navigator.push(
              ctx,
              new MaterialPageRoute(
                builder: (context) => new MainApp(),
              ),
            );
          },
        );
      },
    ),
    title: Container(
        child: Row(
      children: [
        Column(
          children: [
            Container(
              margin: const EdgeInsets.all(3),
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage('assets/img/buu_icon.png'),
                ),
              ),
            )
          ],
        ),
        Column(
          children: [
            Text(
              "มหาวิทยาลัยบูรพา",
              style: TextStyle(fontSize: 20, color: Colors.yellow),
            ),
            Text(
              "Burapha University",
              style: TextStyle(fontSize: 15, color: Colors.white),
            ),
          ],
        ),
      ],
    )),
    backgroundColor: Colors.blue.shade800,
    elevation: 0.0,
  );
}
