import 'package:flutter/material.dart';

class Table_calender extends StatefulWidget {
  @override
  _TableCalender createState() => _TableCalender();
}

class _TableCalender extends State<Table_calender> {  
  @override  
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue.shade800,
        title: const Text("ปฎิทินการศึกษา",style: TextStyle(color: Colors.white)),  
          ),
          
          body: Center(  
              child: Column(children: <Widget>[  
                Container(  
                  margin: EdgeInsets.all(20),  
                  child: Table(  
                    defaultColumnWidth: FixedColumnWidth(120.0),  
                    border: TableBorder.all(  
                        color: Colors.black,  
                        style: BorderStyle.solid,  
                        width: 2),  
                    children: [  
                      TableRow( children: [  
                        Column(children:[Text('รายการ', style: TextStyle(fontSize: 20.0))]),  
                        Column(children:[Text('วันเริ่มต้น', style: TextStyle(fontSize: 20.0))]),  
                        Column(children:[Text('วันสุดท้าย', style: TextStyle(fontSize: 20.0))]),  
                      ]),  
                      TableRow( children: [  
                        Column(children:[Text('- ช่วงจองวิชาลงทะเบียนให้กับนิสิตโดยเจ้าหน้าที่')]),  
                        Column(children:[Text('1 พ.ย. 2565 8:30 น.')]),  
                        Column(children:[Text('18 พ.ย. 2565 23:59 น.')]),  
                      ]),  
                      TableRow( children: [  
                        Column(children:[Text(' - ลงทะเบียนปกติ ( on-line )')]),  
                        Column(children:[Text('11 พ.ย. 2565 8:30 น.')]),  
                        Column(children:[Text('18 พ.ย. 2565 23:59 น.')]),  
                      ]),  
                      TableRow( children: [  
                        Column(children:[Text(' - ชั้นปี 4')]),  
                        Column(children:[Text('11 พ.ย. 2565 8:30 น.')]),  
                        Column(children:[Text('18 พ.ย. 2565 23:59 น.')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text(' - ชั้นปีอื่นๆ')]),  
                        Column(children:[Text('11 พ.ย. 2565 8:30 น.')]),  
                        Column(children:[Text('18 พ.ย. 2565 23:59 น.')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text(' - ชั้นปี 3')]),  
                        Column(children:[Text('14 พ.ย. 2565 8:30 น.')]),  
                        Column(children:[Text('18 พ.ย. 2565 23:59 น.')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text(' - ชั้นปี 2')]),  
                        Column(children:[Text('15 พ.ย. 2565 8:30 น.')]),  
                        Column(children:[Text('18 พ.ย. 2565 23:59 น.')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text(' - ชั้นปี 1')]),  
                        Column(children:[Text('16 พ.ย. 2565 8:30 น.')]),  
                        Column(children:[Text('18 พ.ย. 2565 23:59 น.')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text(' - เริ่มคิดค่าปรับลงทะเบียนล่าช้า')]),  
                        Column(children:[Text('19 พ.ย. 2565 0:00 น.')]),  
                        Column(children:[Text('19 พ.ย. 2565 0:00 น.')]),  
                      ]),
                      TableRow( children: [  
                        Column(children:[Text(' - วันเปิดภาคการศึกษา')]),  
                        Column(children:[Text('19 พ.ย. 2565 0:00 น.')]),  
                        Column(children:[Text('19 พ.ย. 2565 0:00 น.')]),  
                      ]),  
                      TableRow( children: [  
                        Column(children:[Text(' - ช่วงยื่นคำร้องขอขยายเวลาชำระเงิน เนื่องจากขาดแคลนทุนทรัพย์อย่างแท้จริง ที่คณะที่นิสิตสังกัด')]),  
                        Column(children:[Text('19 พ.ย. 2565 0:00 น.')]),  
                        Column(children:[Text('23 ธ.ค. 2565 16:00 น.')]),  
                      ]),
                    ],  
                  ),  
                ),  
              ])  
          ));
  }  
}